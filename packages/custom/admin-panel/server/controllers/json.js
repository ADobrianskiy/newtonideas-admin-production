/**
 * Created by Tania on 6/12/2015.
 */


'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Category = mongoose.model('Category'),
    Product = mongoose.model('Product'),
    Element = mongoose.model('Element'),
    ElementVariation = mongoose.model('ElementVariation');

module.exports = function(AdminPanel) {
    return {
        /**
         * Show all categories in list
         */
        getData: function(req, res, next) {
            res.header('Access-Control-Allow-Origin', "*");
            Category.find(function(err, categories) {
                if (err) return next(err);
                Product.find(function(err, products) {
                    if (err) return next(err);
                    Element.find(function(err, elements) {
                        if (err) return next(err);
                        ElementVariation.find(function(err, elementVariations) {
                            if (err) return next(err);
                            var json = {
                                version: '1.0',
                                currency: '$',
                                backArrow:'resources/back.png',
                                categories: []
                            }
                            categories.forEach(function(item){
                                var prod_of_category = [];
                                if (item.status === "published") {
                                products.forEach(function(product_item){
                                    var elem_of_product = [];
                                    if (product_item.category_id == item._id && product_item.status === "published"){
                                        elements.forEach(function(element_item){
                                            var elVar_of_element = [];
                                            if (element_item.product_id == product_item._id && element_item.status === "published"){
                                                elementVariations.forEach(function(elVar_item){
                                                    if (elVar_item.element_id == element_item._id && elVar_item.status === "published"){
                                                        elVar_of_element.push({
                                                            default: (elVar_item.shown_by_default == 'shown'),
                                                            name: elVar_item.name,
                                                            img: elVar_item.picture,
                                                            preview: elVar_item.icon,
                                                            price: elVar_item.price,
                                                            description: elVar_item.description
                                                        })
                                                    }
                                                });
                                                elem_of_product.push({
                                                    name: element_item.name,
                                                    'z-index': element_item.z_index,
                                                    isMovable: (element_item.movability == 'movable'),
                                                    elementVariations: elVar_of_element
                                                })
                                            }
                                        });
                                        prod_of_category.push({
                                            name: product_item.name,
                                            preview: product_item.icon,
                                            elements: elem_of_product
                                        })
                                    }
                                });
                                json.categories.push({
                                    name: item.name,
                                    preview: item.icon,
                                    products: prod_of_category
                                })
                            }
                            });
                            res.json(json);
                        });
                    });
                });
            });
        }
    };
}

