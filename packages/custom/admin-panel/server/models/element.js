/**
 * Created by Tania on 24/11/2015.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema,
    _   = require('lodash');

/**
 * Getter
 */
var escapeProperty = function(value) {
    return _.escape(value);
};

/**
 * Element Schema
 */

var Element = new Schema({
    product_id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        get: escapeProperty
    },
    status: {
        type: String,
        enum: ['published', 'unpublished'],
        required: true,
        get: escapeProperty
    },
    movability: {
        type: String,
        enum: ['movable', 'immovable'],
        required: true,
        get: escapeProperty
    },
    z_index: {
        type: Number,
        default: 0,
        required: true
    }
});

/**
 * Element Variation Schema
 */

var ElementVariation = new Schema({
    element_id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true,
        get: escapeProperty
    },
    icon: {
        type: String
    },
    picture: {
        type: String
    },
    description: {
        type: String
    },
    price: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        enum: ['published', 'unpublished'],
        required: true,
        get: escapeProperty
    },
    shown_by_default: {
        type: String,
        enum: ['shown', 'hidden'],
        required: true,
        get: escapeProperty
    }
});

mongoose.model('Element', Element);
mongoose.model('ElementVariation', ElementVariation);