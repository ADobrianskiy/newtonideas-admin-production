'use strict';
/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(AdminPanel, app, auth, database) {
    var categories = require('../controllers/categories')(AdminPanel);
    var products = require('../controllers/products')(AdminPanel);
    var elements = require('../controllers/elements')(AdminPanel);
    var elementVariations = require('../controllers/elementVariations')(AdminPanel);
    var upload = require('../controllers/upload')(AdminPanel);
    var json = require('../controllers/json')(AdminPanel);

    app.route('/api/configurator-data')
        .get(json.getData);

    app.all('/api/*', function(req,res,next){
        if (req.originalUrl.match(/^\/api\/public.+/gi)){
            next();
        } else if (!req.isAuthenticated()){
            return res.status(401).send('User is not authorized');
        } else {
            next();
        }
    });

    app.route('/api/categories')
        .get(categories.show);

    app.route('/api/categories')
        .post(categories.add);

    app.route('/api/categories/:id')
        .delete(categories.remove);

    app.route('/api/categories-unpublish')
        .post(categories.unpublish);

    app.route('/api/categories-publish')
        .post(categories.publish);

    app.route('/api/category-name/:category_id')
        .get(categories.name);

    app.route('/api/category-edit/:category_id')
        .get(categories.category);

    app.route('/api/category-edit/:category_id')
        .post(categories.editCategory);

    app.route('/api/category-publishable')
        .get(products.checkPublished);

    app.route('/api/category/:category_id')
        .get(products.show);

    app.route('/api/category/:category_id')
        .post(products.add);

    app.route('/api/category/:id')
        .delete(products.remove);

    app.route('/api/product-unpublish')
        .post(products.unpublish);

    app.route('/api/product-publish')
        .post(products.publish);

    app.route('/api/product-publishable')
        .get(elements.checkPublished);

    app.route('/api/products')
        .get(products.showAll);

    app.route('/api/product-name/:product_id')
        .get(products.name);

    app.route('/api/product-edit/:product_id')
        .get(products.product);

    app.route('/api/product-edit/:product_id')
        .post(products.editProduct);

    app.route('/api/product/:product_id')
        .get(elements.show);

    app.route('/api/product/:product_id')
        .post(elements.add);

    app.route('/api/product/:id')
        .delete(elements.remove);

    app.route('/api/element-unpublish')
        .post(elements.unpublish);

    app.route('/api/element-publish')
        .post(elements.publish);

    app.route('/api/element-publishable')
        .get(elementVariations.checkPublished);

    app.route('/api/element-movable')
        .post(elements.movable);

    app.route('/api/element-immovable')
        .post(elements.immovable);

    app.route('/api/element-name/:element_id')
        .get(elements.name);

    app.route('/api/element-edit/:element_id')
        .get(elements.element);

    app.route('/api/element-edit/:element_id')
        .post(elements.editElement);

    app.route('/api/element/:element_id')
        .get(elementVariations.show);

    app.route('/api/element/:element_id')
        .post(elementVariations.add);

    app.route('/api/element/:id')
        .delete(elementVariations.remove);

    app.route('/api/elementVariation-unpublish')
        .post(elementVariations.unpublish);

    app.route('/api/elementVariation-publish')
        .post(elementVariations.publish);

    app.route('/api/elementVariation-shown')
        .post(elementVariations.shown);

    app.route('/api/elementVariation-hidden')
        .post(elementVariations.hidden);

    app.route('/api/elementVariation-edit/:elementVariation_id')
        .get(elementVariations.elementVariation);

    app.route('/api/elementVariation-edit/:elementVariation_id')
        .post(elementVariations.editElementVariation);

    app.route('/api/upload-img')
        .post(upload.uploadPicture);

    app.route('/api/upload-icon')
        .post(upload.uploadIcon);

    app.get('/api/adminPanel/initialisation', function(req, res, next) {
        res.send('Add new default admin if there is no one in db');
    });

    app.get('/api/adminPanel/example/anyone', function(req, res, next) {
        res.send('Anyone can access this');
    });

    app.get('/api/adminPanel/example/auth', auth.requiresLogin, function(req, res, next) {
        res.send('Only authenticated users can access this');
    });

    app.get('/api/adminPanel/example/admin', auth.requiresAdmin, function(req, res, next) {
        res.send('Only users with Admin role can access this');
    });

    app.get('/api/adminPanel/example/render', function(req, res, next) {
        AdminPanel.render('index', {
            package: 'admin-panel'
        }, function(err, html) {
            //Rendering a view from the Package server/views
            res.send(html);
        });
    });
};
