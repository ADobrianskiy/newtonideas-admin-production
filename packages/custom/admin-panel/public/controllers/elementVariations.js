/**
 * Created by Tania on 4/12/2015.
 */

'use strict';

/* jshint -W098 */
angular.module('mean.admin-panel')
    .controller('EditElementVariationController', ['$scope', '$http', 'multipartForm', 'Global', 'AdminPanel',
        function($scope, $http, multipartForm, Global, AdminPanel) {
            $scope.global = Global;
            $scope.url =  window.location.href;
            if(!$scope.url.endsWith('/')) $scope.url += '/';
            var urlParts = $scope.url.split('/');
            $scope.category_id = urlParts[4];
            $scope.product_id = urlParts[5];
            $scope.elementVariation_id = urlParts[urlParts.length - 3];
            $scope.fileForm = {
                "upload-width": 100,
                "upload-height": 100
            };

            $scope.pictureForm = {};

            var refresh = function() {
                $http.get('/api/elementVariation-edit/' + $scope.elementVariation_id).success(function (response) {
                    $scope.elementVariation = response;
                    $scope.errorMsg = '';
                    $scope.name = response.name;
                });
            };
            refresh();

            $scope.saveElementVariation = function(){
                if (!$scope.elementVariation.name || $scope.elementVariation.price === undefined || !$scope.elementVariation.description) {
                    $scope.errorMsg = "Please, enter name, price and description for this element variation";
                    return;
                }
                if ($scope.elementVariation.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.elementVariation.name = '';
                    return;
                }
                if (isNaN($scope.elementVariation.price)) {
                    $scope.errorMsg = "Please, enter price in numerals";
                    $scope.elementVariation.price = '';
                    return;
                }
                if ($scope.elementVariation.price < 0) {
                    $scope.errorMsg = "Please, enter price above zero";
                    $scope.elementVariation.price = '';
                    return;
                }
                var uploadUrl = "/api/upload-icon";
                var updated = false;
                if ($scope.fileForm.fileData) {
                    updated = true;
                    multipartForm.post(uploadUrl, $scope.fileForm, function(response){
                        $scope.elementVariation.icon = response['thumbs'];
                        $http.post('api/elementVariation-edit/' + $scope.elementVariation_id, $scope.elementVariation)
                            .success(function(response){
                                $scope.saved = response;
                                refresh();
                                if ($scope.pictureForm.fileData) {
                                    multipartForm.post('/api/upload-img', $scope.pictureForm, function(response){
                                        $scope.elementVariation.picture = response['full-size'];
                                        $http.post('api/elementVariation-edit/' + $scope.elementVariation_id, $scope.elementVariation)
                                            .success(function(response){
                                                $scope.saved = response;
                                                refresh();
                                            });
                                    })
                                }
                            });
                    });
                }

                if ($scope.pictureForm.fileData && !updated) {
                    multipartForm.post('/api/upload-img', $scope.pictureForm, function(response){
                        $scope.elementVariation.picture = response['full-size'];
                        $http.post('api/elementVariation-edit/' + $scope.elementVariation_id, $scope.elementVariation)
                            .success(function(response){
                                $scope.saved = response;
                                refresh();
                            });
                    })
                    updated = true;
                }

                if(!updated) {
                    $http.post('api/elementVariation-edit/' + $scope.elementVariation_id, $scope.elementVariation)
                        .success(function (response) {
                            $scope.saved = response;
                            refresh();
                        });
                }
            };

            $scope.unpublish = function(elementVariation){
                AdminPanel.changeStatus(elementVariation, 'elementVariation-unpublish').then(function(){
                    refresh();
                });
            };

            $scope.publish = function(elementVariation){
                AdminPanel.changeStatus(elementVariation, 'elementVariation-publish').then(function(){
                    refresh();
                });
            };

            $scope.shown = function(elementVariation){
                AdminPanel.changeStatus(elementVariation, 'elementVariation-shown').then(function(){
                    refresh();
                });
            };

            $scope.hidden = function(elementVariation){
                AdminPanel.changeStatus(elementVariation, 'elementVariation-hidden').then(function(){
                    refresh();
                });
            }
        }
    ])
    .controller('ElementVariationsController', ['$scope', '$http', 'Global', 'AdminPanel',
        function($scope, $http, Global, AdminPanel) {
            $scope.global = Global;
            $scope.url =  window.location.href;
            if(!$scope.url.endsWith('/')) $scope.url += '/';
            var urlParts = $scope.url.split('/');
            $scope.element_id = urlParts[urlParts.length - 2];

            var refresh = function() {
                $http.get('/api/element/' + $scope.element_id).success(function (response) {
                    $scope.elementVariations = response;
                    if (response.length == 0) $scope.emptyMsg = 'No element variations defined';
                    $scope.elementVariation = '';
                    $scope.errorMsg = '';
                });
                $http.get('/api/element-name/' + $scope.element_id).success(function (response) {
                    $scope.element_name = response;
                });
            };
            refresh();

            $scope.addElementVariation = function(){
                if (!$scope.elementVariation.name || !$scope.elementVariation.price) {
                    $scope.errorMsg = "Please, enter name and price of new element variation";
                    return;
                }
                if ($scope.elementVariation.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.elementVariation.name = '';
                    return;
                }
                if (isNaN($scope.elementVariation.price)) {
                    $scope.errorMsg = "Please, enter price in numerals";
                    $scope.elementVariation.price = '';
                    return;
                }
                if ($scope.elementVariation.price < 0) {
                    $scope.errorMsg = "Please, enter price above zero";
                    $scope.elementVariation.price = '';
                    return;
                }
                $http.post('api/element/' + $scope.element_id, $scope.elementVariation).success(function(response){
                    $scope.emptyMsg = '';
                    refresh();
                    $scope.added = response;
                    $scope.removed = false;
                });
            };

            $scope.removeElementVariation = function(id, name){
                if (confirm('Delete element variation \''+name+'\'?') === true) {
                    $http.delete('api/element/' + id).success(function(response){
                        refresh();
                        $scope.added = false;
                        $scope.removed = name;
                    });
                }
            };

            $scope.unpublish = function(elementVariation){
                AdminPanel.changeStatus(elementVariation, 'elementVariation-unpublish').then(function(){
                    refresh();
                });
            };

            $scope.publish = function(elementVariation){
                AdminPanel.changeStatus(elementVariation, 'elementVariation-publish').then(function(){
                    refresh();
                });
            };

            $scope.shown = function(elementVariation){
                AdminPanel.changeStatus(elementVariation, 'elementVariation-shown').then(function(){
                    refresh();
                });
            };

            $scope.hidden = function(elementVariation){
                AdminPanel.changeStatus(elementVariation, 'elementVariation-hidden').then(function(){
                    refresh();
                });
            }
        }
    ]);

