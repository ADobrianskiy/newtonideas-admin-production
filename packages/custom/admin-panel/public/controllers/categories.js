/**
 * Created by Tania on 4/12/2015.
 */

'use strict';

/* jshint -W098 */
angular.module('mean.admin-panel')
    .controller('CategoriesController', ['$scope', '$interval', '$http', 'Global', 'AdminPanel',
        function($scope, $interval, $http, Global, AdminPanel) {
            var publishableCategories = function() {
                $http.get('/api/category-publishable/').success(function (response) {
                    $scope.publishable = response;
                });
            };
            //var refresh =  $interval(function() {
            //
            //        $http.get('/api/categories').success(function (response) {
            //            $scope.categories = response;
            //            $scope.category = '';
            //            $scope.errorMsg = '';
            //        });
            //        publishableCategories();
            //    }, 2000);

            var refresh =  function() {

                $http.get('/api/categories').success(function (response) {
                    $scope.categories = response;
                    if (response.length == 0) $scope.emptyMsg = 'No categories defined';
                    $scope.category = '';
                    $scope.errorMsg = '';
                });
                AdminPanel.publishableItems('category-publishable/').then(function(response){
                    $scope.publishable = response.data;
                });
            };


            refresh();
            $scope.addCategory = function(){
                if ($scope.category.name === undefined) {
                    $scope.errorMsg = "Please, enter name of new category";
                    return;
                }
                if ($scope.category.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.category.name = '';
                    return;
                }
                $http.post('api/categories', $scope.category).success(function(response){
                    $scope.emptyMsg = '';
                    refresh();
                    $scope.added = response;
                    $scope.removed = false;
                });
            };
            $scope.remove = function(id, name){
                if (confirm('Delete category \''+name+'\'?') === true) {
                    $http.delete('api/categories/' + id).success(function(response){
                        refresh();
                        $scope.added = false;
                        $scope.removed = name;
                    });
                }
            };
            $scope.unpublish = function(category){
                AdminPanel.changeStatus(category, 'categories-unpublish').then(function(){
                    refresh();
                });
            };
            $scope.publish = function(category){
                AdminPanel.changeStatus(category, 'categories-publish').then(function(){
                    refresh();
                });
            };

            $scope.hasPublishedProducts = function(id) {
                if($scope.publishable === undefined) return false;
                return $scope.publishable.indexOf(id) !== -1;
            }
        }
    ])
    .controller('EditCategoryController', ['$scope', '$http', 'multipartForm', 'Global', 'AdminPanel',
        function($scope, $http, multipartForm, Global, AdminPanel) {
            $scope.url =  window.location.href;
            if(!$scope.url.endsWith('/')) $scope.url += '/';
            var urlParts = $scope.url.split('/');
            $scope.category_id = urlParts[urlParts.length - 3];
            $scope.fileForm = {
                "upload-width": 100,
                "upload-height": 100
            };

            var refresh = function() {
                $http.get('/api/category-edit/' + $scope.category_id).success(function (response) {
                    $scope.category = response;
                    $scope.errorMsg = '';
                    $scope.name = response.name;
                    });
                AdminPanel.publishableItems('category-publishable/').then(function(response){
                    $scope.publishable = response.data;
                });
            };
            refresh();

            $scope.saveCategory = function(){
                if (!$scope.category.name) {
                    $scope.errorMsg = "Please, enter name of this category";
                    return;
                }
                if ($scope.category.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.category.name = '';
                    return;
                }
                if (!$scope.fileForm.fileData && $scope.category.icon) {
                    $http.post('api/category-edit/' + $scope.category_id, $scope.category).success(function(response){
                        refresh();
                        $scope.saved = response;
                    });
                    return;
                }
                if (!$scope.fileForm.fileData) {
                    $scope.errorMsg = "Please, upload icon for this category";
                    return;
                }
                var uploadUrl = "/api/upload-img"
                multipartForm.post(uploadUrl, $scope.fileForm, function(response){
                    $scope.category.icon = response['thumbs'];
                    $http.post('api/category-edit/' + $scope.category_id, $scope.category).success(function(response){
                        refresh();
                        $scope.saved = response;
                    });
                })
            };

            $scope.unpublish = function(category){
                AdminPanel.changeStatus(category, 'categories-unpublish').then(function(){
                    refresh();
                });
            };

            $scope.publish = function(category){
                AdminPanel.changeStatus(category, 'categories-publish').then(function(){
                    refresh();
                });
            };

            $scope.hasPublishedProducts = function(id) {
                if($scope.publishable === undefined) return false;
                return $scope.publishable.indexOf(id) !== -1;
            }
        }
    ]);


