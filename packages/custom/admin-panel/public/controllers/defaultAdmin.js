/**
 * Created by Tania on 7/2/2016.
 */

'use strict';

/* jshint -W098 */
angular.module('mean.admin-panel')
    .controller('DefaultAdminController', ['$scope', '$http', '$window',
        function($scope, $http, $window) {
            $scope.addAdmin = function(user) {
                user = user || {};
                if(!user.name || !user.password || !user.email){
                    $scope.errorMsg = "";
                    $scope.warningMsg = "Please, fill all fields before submitting";
                    return;
                }
                $scope.warningMsg = "";
                $scope.errorMsg = "";
                user.username = user.name;
                user.roles = ['admin'];
                user.confirmPassword = user.password;
                $http.post('/api/public/createDefaultAdmin', user).success(function(response){
                    $window.location.href = '/auth/login';
                }).error(function(res){
                    $scope.errorMsg = res[0].msg;
                });
            };

        }
    ]);

