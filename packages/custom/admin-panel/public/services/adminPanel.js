'use strict';

angular.module('mean.admin-panel').factory('AdminPanel', [ '$http',
  function($http) {
    return {
      name: 'admin-panel',
      changeStatus: function(item, link){
          var promise = $http.post('/api/' + link, item).success(function(response){})
          return promise;
      },
      publishableItems: function(link){
          var promise = $http.get('/api/' + link).success(function(response){})
          return promise;
      }
    };
  }
]);
