/**
 * Created by adobrianskiy on 27.11.15.
 */
angular.module('mean.admin-panel').service('multipartForm', ['$http', function($http){
    this.post = function(uploadUrl, data, callback){
        var fd = new FormData();
        for(var key in data){
            fd.append(key, data[key]);
        }

        $http.post(uploadUrl, fd, {
            transformRequest:  angular.identity,
            headers: {'Content-Type': undefined}
        }).success(callback);
    }
}]);