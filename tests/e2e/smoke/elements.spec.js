describe('Elements', function() {
	it('should add an element successfully', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 2. Steps to be executed:
		 1) enter a name of the element
		 2) enter Z-index in numerals
		 3) click on the "Add" button
		 3. Expected result: one more element is
		 added to the elements list
		 */
		browser.get('/');
		protractor.tools.login();
		getToProduct();
		element(by.id('txt-input-name')).sendKeys(protractor.consts.testElement_elemName);
		element(by.id('z-index')).sendKeys(protractor.consts.defaultZindex);
		element(by.id('element-add')).click();
		browser.driver.wait(protractor.until.elementLocated(by.css('.alert-success')));
		expect(element(by.id(protractor.consts.testElement_elemName)).isDisplayed()).toBeTruthy();
	});

	it('should fail adding an element without entering its Z-index', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 2. Steps to be executed:
		 1) enter a name of the element
		 1) click on the "Add" button
		 3. Expected result: proper error must be
		 displayed and prompt to enter a Z-index
		 */
		protractor.tools.login();
		getToProduct();
		element(by.id('txt-input-name')).sendKeys(protractor.consts.testElement_elemName);
		element(by.id('element-add')).click();
		browser.driver.wait(protractor.until.elementLocated(by.css('.alert-danger')));
		expect(element(by.id(protractor.consts.testElement_elemName)).isDisplayed()).toBeTruthy();
	});

	it('should fail adding an element without entering its name', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 2. Steps to be executed:
		 1) enter Z-index in numerals
		 1) click on the "Add" button
		 3. Expected result: proper error must be
		 displayed and prompt to enter a name
		 */
		protractor.tools.login();
		getToProduct();
		element(by.id('z-index')).sendKeys(protractor.consts.defaultZindex);
		element(by.id('element-add')).click();
		browser.driver.wait(protractor.until.elementLocated(by.css('.alert-danger')));
		expect(element(by.id(protractor.consts.testElement_elemName)).isDisplayed()).toBeTruthy();
	});

	it('should fail adding an element without entering any information', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 2. Steps to be executed:
		 1) click on the "Add" button
		 3. Expected result: proper error must be
		 displayed and prompt to enter a name
		 */
		protractor.tools.login();
		getToProduct();
		element(by.id('element-add')).click();
		browser.driver.wait(protractor.until.elementLocated(by.css('.alert-danger')));
		expect(element(by.id(protractor.consts.testElement_elemName)).isDisplayed()).toBeTruthy();
	});


	it('should cancel removing an element', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 2. Steps to be executed:
		 1) click on the "Remove" button of the respective element
		 2) click on the "Cancel" button on the popup window
		 3. Expected result: a respective element isn't removed
		 */
		protractor.tools.login();
		getToProduct();
		element(by.id('element-remove-' + protractor.consts.testElement_elemName)).click();
		browser.switchTo().alert().dismiss();
		expect(element(by.id(protractor.consts.testElement_elemName)).isDisplayed()).toBeTruthy();
	});

	it('should edit an element successfully without changes', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 2. Steps to be executed:
		 1) click on the "Edit" button of the respective element
		 2) click on the "Save" button
		 3. Expected result: a respective element is saved without changes
		 and admin is navigated back to the element list page
		 */
		protractor.tools.login();
		getToProduct();
		element(by.id('element-edit-' + protractor.consts.testElement_elemName)).click();
		var fileToUpload = '../../resources/' + protractor.consts.testElement_image,
			absolutePath = path.resolve(__dirname, fileToUpload);

		element(by.id('upload-file')).sendKeys(absolutePath);
		// browser.driver.wait(protractor.until.elementLocated(by.id('product-save')));
		element(by.id('element-save')).click();
		browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
		element(by.css('.btn-success')).click();
		browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testElement_elemName)));
		expect(element(by.id('element-add')).isPresent()).toBeTruthy();
	});

	it('should edit an element successfully, changing its name', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 2. Steps to be executed:
		 1) click on the "Edit" button of the respective element
		 2) edit its name
		 2) click on the "Save" button
		 3. Expected result: a respective element's name is edited
		//  */
		protractor.tools.login();
		getToProduct();
		element(by.id('element-edit-' + protractor.consts.testElement_elemName)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('element-save')));
		element(by.id('txt-input-name')).clear();
		element(by.id('txt-input-name')).sendKeys(protractor.consts.testElement_elemRename);
		element(by.id('element-save')).click();
		browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
		element(by.css('.btn-success')).click();
		browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testElement_elemRename)));
		expect(element(by.id(protractor.consts.testElement_elemRename)).isPresent()).toBeTruthy();


		element(by.id('element-edit-' + protractor.consts.testElement_elemRename)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('element-save')));
		element(by.id('txt-input-name')).clear();
		element(by.id('txt-input-name')).sendKeys(protractor.consts.testElement_elemName);
		element(by.id('element-save')).click();
		browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
		element(by.css('.btn-success')).click();
		browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testElement_elemName)));

	});

	it('should edit an element successfully, changing its Z-index', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 2. Steps to be executed:
		 1) click on the "Edit" button of the respective element
		 2) edit its Z-index
		 3) click on the "Save" button
		 3. Expected result: a respective element's Z-index is edited
		 */
		protractor.tools.login();
		getToProduct();
		element(by.id('element-edit-' + protractor.consts.testElement_elemName)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('element-save')));
		element(by.id('z-index')).clear();
		element(by.id('z-index')).sendKeys(protractor.consts.defaultZindex);
		element(by.id('element-save')).click();
		browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
        expect(element(by.id('.btn-success')).isPresent()).toBeTruthy();

	});

	it('should return to the elements list after saving an element-s edition', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 5) "Save" button should be clicked on on the edition page
		 2. Steps to be executed:
		 1) Click the "Ok" button on the popup message
		 3. Expected result: admin is navigated back
		 to the elements list page
		 */
		element(by.css('.btn-success')).click();
		browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testElement_elemName)));
		expect(element(by.id(protractor.consts.testElement_elemName)).isPresent()).toBeTruthy();
		//TODO:this one too
	});

	it('should make the element movable', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 5) the element should be immovable
		 2. Steps to be executed:
		 1) click on the "Movability" checkmark of the respective element
		 3. Expected result: the element gets movable movability
		 */
		protractor.tools.login();
		getToProduct();
		element(by.id('move-' + protractor.consts.testElement_elemName)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('immove-' + protractor.consts.testElement_elemName)));
		expect(element(by.id('immove-' + protractor.consts.testElement_elemName)).isDisplayed()).toBeTruthy();

	});

	it('should make the element immovable', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 5) the element should be movable
		 2. Steps to be executed:
		 1) click on the "Movability" checkmark of the respective element
		 3. Expected result: the element gets immovable movability
		 */

		protractor.tools.login();
		getToProduct();
		element(by.id('immove-' + protractor.consts.testElement_elemName)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('move-' + protractor.consts.testElement_elemName)));
		expect(element(by.id('move-' + protractor.consts.testElement_elemName)).isDisplayed()).toBeTruthy();

	});

	it('should make the element published', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 5) at least one element variation should be in the list
		 6) at least one of its element variations should be published
		 7) the element should be unpublished
		 2. Steps to be executed:
		 1) click on the "Status" checkmark of the respective element
		 3. Expected result: the respective element gets published status,
		 therefore its product gets possibility to get published
		 */

		protractor.tools.login();
		getToProduct();
		element(by.id('publish-'+protractor.consts.testElement_pub_unpub_el)).click();
		expect(element(by.id('published-'+protractor.consts.testElement_pub_unpub_el)).isDisplayed()).toBeTruthy();

	});

	it('should make the element unpublished', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 5) at least one element variation should be in the list
		 6) at least one of its element variations should be published
		 7) the element should be published
		 2. Steps to be executed:
		 1) click on the "Status" checkmark of the respective element
		 3. Expected result: the respective element gets unpublished status
		 */
		protractor.tools.login();
		getToProduct();
		element(by.id('unpublish-'+protractor.consts.testElement_pub_unpub_el)).click();
		expect(element(by.id('unpublished-'+protractor.consts.testElement_pub_unpub_el)).isDisplayed()).toBeTruthy();

	});

	it('should remove an element successfully', function () {
		/*
		 1. Precondition:
		 1) admin should be logged in
		 2) at least one category should be in the list
		 3) at least one product should be in the list
		 4) at least one element should be in the list
		 2. Steps to be executed:
		 1) click on the "Remove" button of the respective element
		 2) click on the "Ok" button on the popup window
		 3. Expected result: a respective element is removed
		 */
		protractor.tools.login();
		getToProduct();
		element(by.id('element-remove-' + protractor.consts.testElement_elemName)).click();
		browser.switchTo().alert().accept();
		browser.driver.wait(protractor.until.elementLocated(by.css('.alert-success')));
		expect(element(by.id(protractor.consts.testElement_elemName)).isPresent()).toBeFalsy();

	});

	function getToProduct() {
		browser.get('/categories');
		element(by.id(protractor.consts.testElement_category)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('product-add')));
		element(by.id(protractor.consts.testElement_product)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('element-add')));
	}
});