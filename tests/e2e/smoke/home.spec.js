
describe('Home page', function(){
    it('title should contain AdminPanel', function(){
        browser.get('/');
        expect(browser.getTitle()).toMatch(/.*AdminPanel.*/);
    });

    it('should have no access to categories for unauthorized user', function(){
        logout();

        browser.get('/categories');
        var url = browser.getCurrentUrl();

        expect(url).toMatch(/.*auth\/login.*/);//.then(function(){console.log("*******" + url.promise);});
        url.then(function(resolved) {
            browser.get(resolved);
        });
    });

    it('should have an opportunity to log in successfully', function(){
        logout();
        login();
        expect(element(by.css('.btn-success')).isPresent()).toBeTruthy();
    });

    it('should send admin a mail if he/she has forgotten his/her password', function(){
        logout();
        browser.get('/auth/login');
        element(by.id('forgot-password')).click();
        browser.driver.wait(protractor.until.elementLocated(By.id('email_block')));
        element(by.id('email_block')).sendKeys('admin@admin.ua');
        element(by.id('btn-submit')).click();
        expect(element(by.css('.alert-success')).isDisplayed()).toBeTruthy();

    });

    it('should fail sending admin a mail if he/she has forgotten his/her password', function(){
    /*
	 1. Precondition:
	    1) at least one admin should be registered
     2. Steps to be executed:
        1) click on the "Forgot you password?"
        2) enter email address that is not registered
     3. Expected result: proper error must be
        displayed
    */
        logout();
        browser.get('/auth/login');
        element(by.id('forgot-password')).click();
        browser.driver.wait(protractor.until.elementLocated(By.id('email_block')));
        element(by.id('email_block')).sendKeys('unregistred@admin.ua');
        element(by.id('btn-submit')).click();
        expect(element(by.css('.alert-danger')).isDisplayed()).toBeTruthy();


    })

    it('should get access to categories', function(){
    /*
     1. Precondition:
        1) admin should be logged in
     2. Steps to be executed:
        1) click on the "Here" button
     3. Expected result: admin is navigated to the
	    categories list page
    */
        login();
        browser.get('/categories', 4000);
        var url = browser.getCurrentUrl();
        expect(url).toMatch(/.*categories.*/)
        //browser.driver.sleep(5000);



    });

    function login()
    {
        element(by.id('log-in')).isDisplayed().then(function(result) {
            if ( result ) {
                browser.get('/auth/login');
                element(by.id('email_block')).sendKeys('admin@admin.ua');
                element(by.id('password_block')).sendKeys('123456789');
                element(by.css('.submit_button')).click();
                browser.driver.wait(protractor.until.elementLocated(By.css('.btn-success')));
            } else {
                //Whatever if it is false (not displayed)
            }
        });
    }

    function logout()
    {
        element(by.id('log-out')).isDisplayed().then(function(result) {
            if ( result ) {
                element(by.id('log-out')).click();
                browser.driver.wait(protractor.until.elementLocated(By.id('log-in')));
            } else {
                //Whatever if it is false (not displayed)
            }
        });
    }
});